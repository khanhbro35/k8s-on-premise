module "network" {
  source         = "./modules/network"
  cidr_block     = "10.0.0.0/16"
  zone           = ["ap-southeast-1a", "ap-southeast-1b"]
  subnet-public  = ["10.0.1.0/24"]
  subnet-private = ["10.0.2.0/24"]
}

module "compute" {
  source        = "./modules/compute"
  ami           = "ami-0ff89c4ce7de192ea"
  instance_type = "t2.medium"
  depends_on    = [module.network]
}

output "private-key" {
  value     = module.compute.private-key
  sensitive = true
}