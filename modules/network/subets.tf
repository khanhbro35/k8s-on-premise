resource "aws_subnet" "subnet-public" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = var.subnet-public[count.index]
  availability_zone       = var.zone[0]
  map_public_ip_on_launch = true
  count                   = length(var.subnet-public)
  tags = {
    "Name" = "subnet-public-${count.index + 1}"
  }

}

resource "aws_subnet" "subnet-private" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.subnet-private[count.index]
  availability_zone = var.zone[1]
  count             = length(var.subnet-private)
  tags = {
    "Name" = "subnet-private-${count.index + 1}"
  }
}