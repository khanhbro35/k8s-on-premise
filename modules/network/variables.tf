variable "cidr_block" {
  type = string
}

variable "zone" {
  type = list(any)
}

variable "subnet-public" {
  type = list(any)
}

variable "subnet-private" {
  type = list(any)
}