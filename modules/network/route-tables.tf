# public
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.vpc.id
}
resource "aws_route_table" "table-public" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
  }
}

resource "aws_route_table_association" "table-public-attach" {
  subnet_id      = aws_subnet.subnet-public[count.index].id
  route_table_id = aws_route_table.table-public.id
  count          = length(var.subnet-public)
}

#private

resource "aws_eip" "nat-ip" {
  vpc = true
}

resource "aws_nat_gateway" "nat-gateway" {
  allocation_id = aws_eip.nat-ip.id
  subnet_id     = aws_subnet.subnet-public[0].id
}

resource "aws_route_table" "table-private" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat-gateway.id
  }
}

resource "aws_route_table_association" "table-private-attach" {
  subnet_id      = aws_subnet.subnet-private[count.index].id
  route_table_id = aws_route_table.table-private.id
  count          = length(var.subnet-private)
}