data "aws_subnet" "subnet-public" {
  filter {
    name   = "tag:Name"
    values = ["subnet-public-${count.index + 1}"]
  }
  count = 1
}

data "aws_subnet" "subnet-private" {
  filter {
    name   = "tag:Name"
    values = ["subnet-private-${count.index + 1}"]
  }
  count = 1
}

data "aws_security_group" "bastion-host" {
  filter {
    name   = "tag:Name"
    values = ["bastion-host"]
  }
}

data "aws_security_group" "node-sg" {
  filter {
    name   = "tag:Name"
    values = ["node-sg"]
  }
}