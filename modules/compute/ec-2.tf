resource "aws_instance" "bastion-host" {
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = aws_key_pair.ssh.key_name
  associate_public_ip_address = true
  subnet_id                   = data.aws_subnet.subnet-public[count.index].id
  security_groups             = [data.aws_security_group.bastion-host.id]
  count                       = length(data.aws_subnet.subnet-public)
  tags = {
    "Name" = "bastion-hos"
  }
}

resource "aws_instance" "node-master" {
  ami             = var.ami
  instance_type   = var.instance_type
  key_name        = aws_key_pair.ssh.key_name
  subnet_id       = data.aws_subnet.subnet-private[count.index].id
  security_groups = [data.aws_security_group.node-sg.id]
  count           = length(data.aws_subnet.subnet-private)
  tags = {
    "Name" = "node-master"
  }
}

resource "aws_instance" "node-woker-1" {
  ami             = var.ami
  instance_type   = var.instance_type
  key_name        = aws_key_pair.ssh.key_name
  subnet_id       = data.aws_subnet.subnet-private[count.index].id
  security_groups = [data.aws_security_group.node-sg.id]
  count           = length(data.aws_subnet.subnet-private)
  tags = {
    "Name" = "node-woker-1"
  }
}

resource "aws_instance" "node-woker-2" {
  ami             = var.ami
  instance_type   = var.instance_type
  key_name        = aws_key_pair.ssh.key_name
  subnet_id       = data.aws_subnet.subnet-private[count.index].id
  security_groups = [data.aws_security_group.node-sg.id]
  count           = length(data.aws_subnet.subnet-private)
  tags = {
    "Name" = "node-woker-2"
  }
}